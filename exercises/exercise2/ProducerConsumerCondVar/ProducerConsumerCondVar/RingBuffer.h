/**
 * Copyright 2019 Frank Duerr
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RING_BUFEFR_H
#define RING_BUFFER_H

/**
 * Ring-Puffer (FIFO) Datenstruktur.
 */
struct ring_buffer
{
	void** buffer;          // der Puffer (Array von void*-Zeigern)
	size_t buffer_size;     // Gr��e des Puffers (maximale Anzahl Elemente im Puffer)
	size_t pos_head = 0;    // n�chste Einf�geposition
	size_t pos_tail = 0;    // n�chste Entnahmeposition
	size_t count = 0;       // aktuelle Anzahl Element im Puffer
};

/**
 * Initialisiere einen Ringpuffer vor der ersten Verwendung.
 *
 * @param rb zu initialisierender Ring-Puffer.
 * @param size Gr��e des Ringpuffers (maximale Anzahl Elemente im Puffer).
 */
void ring_buffer_init(struct ring_buffer *rb, size_t size);

/**
 * Gibt Speicher eines Ring-Puffers wieder frei.
 *
 * @param rb freizugebender Ring-Puffer.
 */
void ring_buffer_free(struct ring_buffer* rb);

/**
 * F�ge dem Ringpuffer ein Element hinzu.
 * Diese Funktion ist nicht thread-safe! Es ist Aufgabe des Aufrufers, nebenl�ufige Aufrufe zu synchronisieren.
 *
 * @param item Zeiger auf hinzuzuf�gendes Element.
 * @return true, falls das Element hinzugef�gt werden konnte; false, falls der Puffer bereits voll ist.
 */
bool ring_buffer_add_head(struct ring_buffer* rb, void* item);

/**
 * Entnehme das letzte hinzugef�gte Element aus dem Ringpuffer.
 * Diese Funktion ist nicht thread-safe! Es ist Aufgabe des Aufrufers, nebenl�ufige Aufrufe zu synchronisieren.
 * 
 * @return Zeiger auf entnommenes Element; NULL, wenn der Ring-Puffer leer ist.
 */
void *ring_buffer_remove_tail(struct ring_buffer* rb);

/**
 * Liefert die aktuelle Anzahl an Elementen im Puffer.
 * Diese Funktion ist nicht thread-safe! Es ist Aufgabe des Aufrufers, nebenl�ufige Aufrufe zu synchronisieren.
 *
 * @return Anzahl Elemente im Puffer
 */
size_t ring_buffer_count(struct ring_buffer* rb);

/**
 * Liefert die Gr��e des Ringpuffers (maximale Anzahl im Puffer).
 *
 * @return Ringpuffer Gr��e.
 */
size_t ring_buffer_size(struct ring_buffer* rb);

#endif
