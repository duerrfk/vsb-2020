/**
 * Copyright 2019 Frank Duerr
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "RingBuffer.h"
#include <stdlib.h>
#include <stdio.h>
#include <cassert>
#include <Windows.h>

void ring_buffer_init(struct ring_buffer* rb, size_t size)
{
	rb->buffer = (void **) malloc(sizeof(void*) * size);
	if (rb->buffer == NULL) {
		fprintf(stderr, "Could not allocate memory for buffer (%d). \n", GetLastError());
		exit(1);
	}
	rb->buffer_size = size;
	rb->pos_head = 0;
	rb->pos_tail = 0;
	rb->count = 0;
}

void ring_buffer_free(struct ring_buffer* rb)
{
	assert(rb != NULL);

	free(rb->buffer);
}

bool ring_buffer_add_head(struct ring_buffer* rb, void* item)
{
	assert(rb != NULL);

	// Ring-Puffer schon voll?
	if (rb->count == rb->buffer_size)
		return false;

	// Ring-Puffer nicht voll.

	rb->buffer[rb->pos_head] = item;
	rb->pos_head = (rb->pos_head + 1) % rb->buffer_size;
    rb->count++;

	return true;
}

void* ring_buffer_remove_tail(struct ring_buffer* rb)
{
	assert(rb != NULL);

	if (rb->count == 0)
		return NULL;

	// Ring-Puffer nicht leer.

	void* item = rb->buffer[rb->pos_tail];
	rb->pos_tail = (rb->pos_tail + 1) % rb->buffer_size;
	rb->count--;

	return item;
}

size_t ring_buffer_count(struct ring_buffer* rb)
{
	assert(rb != NULL);

	return rb->count;
}

size_t ring_buffer_size(struct ring_buffer* rb)
{
	assert(rb != NULL);

	return rb->buffer_size;
}