/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <windows.h>
#include <stdio.h>

HANDLE  hThread1, hThread2;
DWORD   thread1Id, thread2Id;

// Threads execute this function.
DWORD WINAPI thread_run(LPVOID lpParam)
{
	int *numbers = (int *) lpParam;
	int x = numbers[0];
	int y = numbers[1];
	int res = x+y;
	printf("Thread: %d + %d = %d \n", x, y, res);
	
	return 0;
}

int main(int argc, char *argv[])
{
	// Create Thread 1.
	int args1[] = {1,2};
	hThread1 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread_run,             // thread function
		&args1,                 // arguments to thread function 
		0,                      // use default creation flags 
		&thread1Id);            // receives the thread identifier 
	if (hThread1 == NULL) {
		printf("Could not create thread (%d).\n", GetLastError());
		return 1;
	}

	// Create Thread 2.
	int args2[] = {42,43};
	hThread2 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread_run,             // thread function
		&args2,                 // arguments to thread function 
		0,                      // use default creation flags 
		&thread2Id);            // receives the thread identifier 
	if (hThread2 == NULL) {
		printf("Could not create thread (%d).\n", GetLastError());
		return 1;
	}

	// Wait for (join) Thread 1 and Thread 2.
	DWORD res1 = WaitForSingleObject(hThread1, INFINITE);
	if (res1 == WAIT_FAILED) {
		fprintf(stderr, "Could not join Thread 1. \n");
	}
	DWORD res2 = WaitForSingleObject(hThread2, INFINITE);
	if (res2 == WAIT_FAILED) {
		fprintf(stderr, "Could not join Thread 2. \n");
	}

	return 0;
}

