#ifndef STDTHREADS_WRAPPER_H
#define STDTHREADS_WRAPPER_H

#include <thread>

enum {
	thrd_success,
	thrd_nomem,
	thrd_timedout,
	thrd_busy,
	thrd_error
};
typedef std::shared_ptr<std::thread> thrd_t;
typedef int (*thrd_start_t)(void*);

int thrd_create(thrd_t* thr, thrd_start_t func, void* args);
int thrd_join(thrd_t thr, int* res);

#endif
