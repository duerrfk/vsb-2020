/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <pthread.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

pthread_t thread1, thread2;

volatile int data = 0;
volatile atomic_bool ready;

// Thread 1
void *thread1_run(void *args)
{
     data = 42;
     atomic_store(&ready, true);
     // With explicit memory order model
     //atomic_store_explicit(&ready, true, memory_order_seq_cst);
     
     return NULL;
}

// Thread 2
void *thread2_run(void *args)
{
     // Busy waiting for data from Thread 1 to be ready.
     while (!atomic_load(&ready));
     // With explicit memory order model
     //while(!atomic_load_explicit(&ready, memory_order_seq_cst));
     
     data += 44;
     
     return NULL;
}

int main(int argc, char *argv[])
{
     // Start Thread 1.
     int success = pthread_create(&thread1, NULL, thread1_run, NULL);
     if (success != 0) {
	  perror("Could not start Thread 1");
	  exit(1);
     }

     // Start Thread 2.
     success = pthread_create(&thread2, NULL, thread2_run, NULL);
     if (success != 0) {
	  perror("Could not start Thread 2");
	  exit(1);
     }

     // Wait for Thread 1 and Thread 2 to finish.
     success = pthread_join(thread1, NULL);
     if (success != 0) {
	  perror("Could not join Thread 1");
	  exit(1);
     }
     success = pthread_join(thread2, NULL);
     if (success != 0) {
	  perror("Could not join Thread 2");
	  exit(1);
     }

     printf("Data is %d \n", data);
     
     return 0;
}
