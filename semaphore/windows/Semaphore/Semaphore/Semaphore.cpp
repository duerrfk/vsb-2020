/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Threads
HANDLE thread1, thread2;

// Binary semaphore for mutual exclusion of critical section
HANDLE semaphore;

DWORD WINAPI thread_run(LPVOID args)
{
	int thread_number = *((int *) args);

	while (true) {
	        // Wait for binary semaphore before entering critical section.
		DWORD success  = WaitForSingleObject(semaphore, INFINITE);
		if (success == WAIT_FAILED) {
			fprintf(stderr, "Could not wait for semaphore (%d).\n", GetLastError());
			return 0; // exit thread
		}

		printf("Thread %d in critical section. \n", thread_number);

		//  Signal semaphore after leaving critical section.
		BOOL bsuccess = ReleaseSemaphore(
			semaphore,   // Semaphore to leave/release/signal
			1,           // Release count
			NULL         // Previous count
		);
		if (!bsuccess) {
			fprintf(stderr, "Could not release semaphore (%d).\n", GetLastError());
			return 1; // exit thread
		}
	}

	return 0;
}

int main(int argc, char* argv[])
{
	// Create a binary semaphore.
	semaphore = CreateSemaphore(
		NULL,         // default security attributes
		1,            // initial semaphore count (one thread can pass)
		1,            // max. semaphore count (1 for binary sem.)
		NULL          // unnamed semaphore
	);
	if (semaphore == NULL) {
		fprintf(stderr, "Could not create semaphore (%d) \n", GetLastError());
		return 1;
	}

	// Create Thread 1.
	int arg1 = 1; // thread number as argument to thread function
    thread1 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread_run,             // thread function
		&arg1,                   // arguments to thread function 
		0,                      // use default creation flags 
		NULL);                  // don't need the thread identifier 
	if (thread1 == NULL) {
		fprintf(stderr, "Could not create Thread 1 (%d). \n", GetLastError());
		return 1;
	}

	// Create Thread 2.
	int arg2 = 2; // thread number as argument to thread function
	thread2 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread_run,             // thread function
		&arg2,                   // arguments to thread function 
		0,                      // use default creation flags 
		NULL);                  // don't need the thread identifier 
	if (thread2 == NULL) {
		fprintf(stderr, "Could not create Thread 2 (%d). \n", GetLastError());
		return 1;
	}

	// Wait for Thread 1.
	DWORD success = WaitForSingleObject(thread1, INFINITE);
	if (success == WAIT_FAILED) {
		fprintf(stderr, "Could not join Thread 1 (%d).\n", GetLastError());
		return 1;
	}

	// Wait for Thread 2.
	success = WaitForSingleObject(thread2, INFINITE);
	if (success == WAIT_FAILED) {
		fprintf(stderr, "Could not join Thread 2 (%d).\n", GetLastError());
		return 1;
	}

	return 0;
}
