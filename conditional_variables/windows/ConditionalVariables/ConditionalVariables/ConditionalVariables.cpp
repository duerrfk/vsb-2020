/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <windows.h>
#include <stdio.h>

HANDLE thread1, thread2;

CRITICAL_SECTION cs; // a critical section is actually a special MUTEX
CONDITION_VARIABLE ready1, ready2;

volatile int number;

DWORD WINAPI thread1_run(LPVOID lpParam)
{
	while (true) {
		EnterCriticalSection(&cs);

		// Critical section: this thread tries to change a 1 to 0.

		// Wait for number to become 1.
		// While-loop protects from spurious wakeups
		// (wakeup without someone calling signal).
		while (number != 1) {
			// Wait for other thread to set number to 1.
			// The thread calling cnd_wait() must hold the lock.
			// cnd_wait() will automatically release the lock.
			SleepConditionVariableCS(&ready2, &cs, INFINITE);
			// This thread again holds the lock here.
		}

		// Number is now 1 -> set it to 0.
		number = 0;
		printf("Thread 1: number = %d \n", number);

		// Signal Thread 2 that Thread 1 has set number to 0.
	    // It's OK to signal before or after the mutex is unlocked.
		WakeConditionVariable(&ready1);

		LeaveCriticalSection(&cs);
	}

	return 0;
}

DWORD WINAPI thread2_run(LPVOID lpParam)
{
	while (true) {
		EnterCriticalSection(&cs);

		// Critical section: this thread tries to change a 0 to 1.

		// Wait for number to become 0.
		// While-loop protects from spurious wakeups
		// (wakeup without someone calling signal).
		while (number != 0) {
			// Wait for other thread to set number to 0.
			// The thread calling cnd_wait() must hold the lock.
			// cnd_wait() will automatically release the lock.
			SleepConditionVariableCS(&ready1, &cs, INFINITE);
			// This thread again holds the lock here.
		}

		// Number is now 0 -> set it to 1.
		number = 1;
		printf("Thread 2: number = %d \n", number);

		// Signal Thread 1 that Thread 2 has set number to 1.
		// It's OK to signal before or after the mutex is unlocked.
		WakeConditionVariable(&ready2);

		LeaveCriticalSection(&cs);
	}

	return 0;
}

int main(int argc, char* argv[])
{
	// Create critical section (MUTEX) object
	InitializeCriticalSection(&cs);

	// Create conditonal variables
	InitializeConditionVariable(&ready1);
	InitializeConditionVariable(&ready2);
	
	// Create Thread 1
	thread1 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread1_run,            // thread function
		NULL,                   // no arguments to thread function 
		0,                      // use default creation flags 
		NULL);                  // don't receive the thread identifier 
	if (thread1 == NULL) {
		printf("Could not create thread (%d).\n", GetLastError());
		return 1;
	}

	// Create Thread 2
	int args2[] = { 42,43 };
	thread2 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread2_run,            // thread function
		NULL,                   // no arguments to thread function 
		0,                      // use default creation flags 
		NULL);            // don't receive the thread identifier 
	if (thread2 == NULL) {
		printf("Could not create thread (%d).\n", GetLastError());
		return 1;
	}

	// Wait for (join) Thread 1 and Thread 2.
	DWORD res1 = WaitForSingleObject(thread1, INFINITE);
	if (res1 == WAIT_FAILED) {
		fprintf(stderr, "Could not join Thread 1. \n");
	}
	DWORD res2 = WaitForSingleObject(thread2, INFINITE);
	if (res2 == WAIT_FAILED) {
		fprintf(stderr, "Could not join Thread 2. \n");
	}

	return 0;
}
